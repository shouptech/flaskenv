# flaskenv

A demo Flask web app that prints out system environment variables. This is
purely for demonstration.

## Launching dev server

```bash
$ pip install -r requirements.txt
$ ./wsgi.py
```

## Launching docker container

```bash
$ docker run -d --name flaskenv -p 8000:8000 registry.gitlab.com/shouptech/flaskenv
$ curl localhost:8000
