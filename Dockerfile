FROM python:3-alpine

WORKDIR /app
COPY requirements.txt .
COPY flaskenv flaskenv/
COPY wsgi.py .

RUN pip install -r requirements.txt

EXPOSE 8000/tcp

CMD gunicorn -b [::]:8000 wsgi:app
