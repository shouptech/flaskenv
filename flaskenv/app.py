# -*- coding: utf-8 -*-
"""
App views
"""

from os import environ
from flask import Flask
from flask import render_template
from flask import jsonify

app = Flask(__name__)

@app.route('/')
def printenv():
    return render_template('printenv.html', environ=environ)

@app.route('/json')
def jsonenv():
    return jsonify(dict(environ))

@app.route('/status')
def status():
    return 'ok'
